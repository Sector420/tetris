package com.example

import javafx.animation.AnimationTimer
import javafx.application.Application
import javafx.event.EventHandler
import javafx.scene.Group
import javafx.scene.Scene
import javafx.scene.canvas.Canvas
import javafx.scene.canvas.GraphicsContext
import javafx.scene.input.KeyCode
import javafx.scene.paint.Color
import javafx.stage.Stage
import kotlin.random.Random

class TetrisApplication : Application() {

    companion object {
        private const val WIDTH = 380
        private const val HEIGHT = 664
    }

    private lateinit var mainScene: Scene
    private lateinit var graphicsContext: GraphicsContext

    private val currentlyActiveKeys = mutableSetOf<KeyCode>()

    private val game = Game()

    override fun start(mainStage: Stage) {
        mainStage.title = "Tetris"

        val root = Group()
        mainScene = Scene(root)
        mainStage.scene = mainScene

        val canvas = Canvas(WIDTH.toDouble(), HEIGHT.toDouble())
        root.children.add(canvas)

        prepareActionHandlers()

        graphicsContext = canvas.graphicsContext2D

        game.blocks.add(Jblock(game.field.field))

        object : AnimationTimer() {
            private var num = 0
            private var ticks = 0
            private var ticks2 = 0
            override fun handle(currentNanoTime: Long) {
                if(game.blocks[num].done) {
                    game.cur += 1
                    when (Random.nextInt(0,7)) {
                        0 -> {
                            game.blocks.add(Oblock(game.field.field))
                        }
                        1 -> {
                            game.blocks.add(Iblock(game.field.field))
                        }
                        2 -> {
                            game.blocks.add(Jblock(game.field.field))
                        }
                        3 -> {
                            game.blocks.add(Lblock(game.field.field))
                        }
                        4 -> {
                            game.blocks.add(Sblock(game.field.field))
                        }
                        5 -> {
                            game.blocks.add(Zblock(game.field.field))
                        }
                        else -> {
                            game.blocks.add(Tblock(game.field.field))
                        }
                    }
                    num+=1
                }
                ticks = (ticks + 1) % 50
                if (ticks == 0) {
                    game.tick()
                }
                ticks2 = (ticks2 + 1) % 6
                if (ticks2 == 0) {
                    updatable()
                }
                game.renderScene(canvas)
                if(game.field.field[0][5]!=0) {
                    this.stop()
                    canvas.graphicsContext2D.strokeText("Game over your point is "+game.blocks.size, 130.0, 300.0)
                }
            }
        }.start()

        mainStage.show()
    }

    private fun prepareActionHandlers() {
        mainScene.onKeyPressed = EventHandler { event ->
            currentlyActiveKeys.add(event.code)
        }
        mainScene.onKeyReleased = EventHandler { event ->
            currentlyActiveKeys.remove(event.code)
        }
    }

    private fun updatable() {
        if (currentlyActiveKeys.contains(KeyCode.LEFT)) {
            game.left()
        }
        if (currentlyActiveKeys.contains(KeyCode.RIGHT)) {
            game.right()
        }
        if (currentlyActiveKeys.contains(KeyCode.UP)) {
            game.rotate()
        }
        if (currentlyActiveKeys.contains(KeyCode.DOWN)) {
            game.tick()
        }
    }
}

interface Renderable {
    fun render(canvas: Canvas)
}

class Game {
    val blocks = mutableListOf<Blocks>()
    var field = Field()
    var cur = 0

    fun tick() {
        if(!blocks[cur].done){
            blocks[cur].step()
        }
    }

    fun rotate() {
        if(!blocks[cur].done) {
            blocks[cur].rotate()
        }
    }

    fun left() {
        if(!blocks[cur].done) {
            blocks[cur].moveleft()
        }
    }

    fun right() {
        if(!blocks[cur].done) {
            blocks[cur].moveright()
        }
    }

    fun renderScene(canvas: Canvas) {
        var c = 0
        canvas.graphicsContext2D.fill = Color.BLACK
        canvas.graphicsContext2D.fillRect(0.0, 0.0, canvas.width, canvas.height)
        for(i in 0 until 21) {
            for (j in 0 until 12) {
                if(field.field[i][j]!=0 && field.field[i][j]!=1) {
                    c+=1
                }
            }
            if(c==10) {
               for (z in i downTo 1) {
                   for (j in 1 until 11) {
                       field.field[z][j]=field.field[z-1][j]
                   }
               }
            }
            c=0
        }
        for(i in 0 until 21) {
            for (j in 0 until 12) {
                when {
                    field.field[i][j]==0 -> {
                        canvas.graphicsContext2D.fill = Color.WHITE
                    }
                    field.field[i][j]==1 -> {
                        canvas.graphicsContext2D.fill = Color.BROWN
                    }
                    field.field[i][j]==2 -> {
                        canvas.graphicsContext2D.fill = Color.YELLOW
                    }
                    field.field[i][j]==3 -> {
                        canvas.graphicsContext2D.fill = Color.CYAN
                    }
                    field.field[i][j]==4 -> {
                        canvas.graphicsContext2D.fill = Color.BLUE
                    }
                    field.field[i][j]==5 -> {
                        canvas.graphicsContext2D.fill = Color.ORANGE
                    }
                    field.field[i][j]==6 -> {
                        canvas.graphicsContext2D.fill = Color.GREEN
                    }
                    field.field[i][j]==7 -> {
                        canvas.graphicsContext2D.fill = Color.RED
                    }
                    else -> {
                        canvas.graphicsContext2D.fill = Color.PURPLE
                    }
                }
                canvas.graphicsContext2D.fillRect((j+1)*1.5+(j*30), (i+1)*1.5+(i*30), 30.0, 30.0)
            }
        }
        for (block in blocks) {
            if (block is Renderable && !block.done) {
                block.render(canvas)
            }
        }
    }
}
class Field {
    var field = arrayOf(
        arrayOf(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
        arrayOf(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
        arrayOf(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
        arrayOf(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
        arrayOf(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
        arrayOf(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
        arrayOf(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
        arrayOf(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
        arrayOf(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
        arrayOf(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
        arrayOf(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
        arrayOf(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
        arrayOf(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
        arrayOf(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
        arrayOf(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
        arrayOf(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
        arrayOf(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
        arrayOf(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
        arrayOf(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
        arrayOf(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
        arrayOf(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
    )
}