package com.example

import javafx.scene.canvas.Canvas
import javafx.scene.paint.Color

sealed class Blocks(var field: Array<Array<Int>>) {
    companion object {
        const val WIDTH = 30.0
        const val HEIGHT = 30.0
    }
    var row = 0
    var column = 5
    var done = false
    var rc = 0
    abstract fun step()
    abstract fun moveleft()
    abstract fun moveright()
    abstract fun movabletoleft(): Boolean
    abstract fun movabletoright(): Boolean
    abstract fun rotate()
}

class Oblock(field: Array<Array<Int>>) : Renderable, Blocks(field) {

    override fun render(canvas: Canvas) {
        val context = canvas.graphicsContext2D
        context.fill = Color.YELLOW

        context.fillRect((column+1)*1.5+(column*30), (row+1)*1.5+(row*30), WIDTH, HEIGHT)
        context.fillRect((column+1)*1.5+(column*30), ((row+1)+1)*1.5+((row+1)*30), WIDTH, HEIGHT)
        context.fillRect(((column+1)+1)*1.5+((column+1)*30), (row+1)*1.5+(row*30), WIDTH, HEIGHT)
        context.fillRect(((column+1)+1)*1.5+((column+1)*30), ((row+1)+1)*1.5+((row+1)*30), WIDTH, HEIGHT)
    }

    override fun step() {
        if(field[row+2][column]==0 && field[row+2][column+1]==0) {
            row+=1
        }
        else {
            done = true
            field[row][column]=2
            field[row][column+1]=2
            field[row+1][column]=2
            field[row+1][column+1]=2
        }
    }

    override fun movabletoleft(): Boolean {
        return field[row][column-1]==0 && field[row+1][column-1]==0
    }

    override fun movabletoright(): Boolean {
        return field[row][column+2]==0 && field[row+1][column+2]==0
    }

    override fun moveleft() {
        if(movabletoleft()) {
            column -= 1
        }
    }

    override fun moveright() {
        if(movabletoright()) {
            column += 1
        }
    }

    override fun rotate() {}
}

class Iblock(field: Array<Array<Int>>) : Renderable, Blocks(field) {

    override fun render(canvas: Canvas) {
        val context = canvas.graphicsContext2D
        context.fill = Color.CYAN
        if(rc % 2 ==0) {
            for(i in 0 until 4) {
                context.fillRect((column+1)*1.5+(column*30), ((row+i)+1)*1.5+((row+i)*30), WIDTH, HEIGHT)
            }
        }
        else {
            for(i in 0 until 4) {
                context.fillRect(((column+i)+1)*1.5+((column+i)*30), (row+1)*1.5+(row*30), WIDTH, HEIGHT)
            }
        }
    }

    override fun step() {
        if(rc % 2 == 0 && field[row+4][column]==0) {
            row+=1
        }
        else if (rc % 2 == 1 && field[row+1][column]==0 && field[row+1][column+1]==0 && field[row+1][column+2]==0 && field[row+1][column+3]==0) {
            row+=1
        }
        else {
            done = true
            if(rc % 2 == 0){
                field[row][column]=3
                field[row+1][column]=3
                field[row+2][column]=3
                field[row+3][column]=3
            }
            else {
                field[row][column]=3
                field[row][column+1]=3
                field[row][column+2]=3
                field[row][column+3]=3
            }

        }
    }

    override fun movabletoleft(): Boolean {
        return if(rc % 2 == 0 && field[row][column-1]==0 && field[row+1][column-1]==0 && field[row+2][column-1]==0 && field[row+3][column-1]==0) {
            true
        } else rc % 2 == 1 && field[row][column-1]==0
    }

    override fun movabletoright(): Boolean {
        return if(rc % 2 == 0 && field[row][column+1]==0 && field[row+1][column+1]==0 && field[row+2][column+1]==0 && field[row+3][column+1]==0) {
            true
        } else rc % 2 == 1 && field[row][column+4]==0
    }

    override fun moveleft() {
        if(movabletoleft()) {
            column -= 1
        }
    }

    override fun moveright() {
        if(movabletoright()) {
            column += 1
        }
    }

    override fun rotate() {
        if(!(rc % 2 == 0 && (field[row][column-1]!=0 || field[row][column+1]!=0 || field[row][column+2]!=0))) {
            rc += 1
            if(rc % 2 == 1) {
                row += 1
                column -= 1
            }
            else if(field[row+1][column]!=1){
                row -= 1
                column += 1
            }
        }
    }
}

class Jblock(field: Array<Array<Int>>) : Renderable, Blocks(field) {

    override fun render(canvas: Canvas) {
        val context = canvas.graphicsContext2D
        context.fill = Color.BLUE
        context.fillRect((column+1)*1.5+(column*30), (row+1)*1.5+(row*30), WIDTH, HEIGHT)
        when {
            rc % 4 == 0 -> {
                for(i in 0 until 3) {
                    context.fillRect(((column+i)+1)*1.5+((column+i)*30), ((row+1)+1)*1.5+((row+1)*30), WIDTH, HEIGHT)
                }
            }
            rc % 4 == 1 -> {
                for(i in 0 until 3) {
                    context.fillRect(((column-1)+1)*1.5+((column-1)*30), ((row+i)+1)*1.5+((row+i)*30), WIDTH, HEIGHT)
                }
            }
            rc % 4 == 2 -> {
                for(i in 0 until 3) {
                    context.fillRect(((column-i)+1)*1.5+((column-i)*30), ((row-1)+1)*1.5+((row-1)*30), WIDTH, HEIGHT)
                }
            }
            rc % 4 == 3 -> {
                for(i in 0 until 3) {
                    context.fillRect(((column+1)+1)*1.5+((column+1)*30), ((row-i)+1)*1.5+((row-i)*30), WIDTH, HEIGHT)
                }
            }
        }
    }

    override fun step() {
        when {
            rc % 4 == 0 && field[row+2][column]==0 && field[row+2][column+1]==0 && field[row+2][column+2]==0 -> {
                row+=1
            }
            rc % 4 == 1 && field[row+3][column-1]==0 && field[row+1][column]==0 -> {
                row+=1
            }
            rc % 4 == 2 && field[row][column-2]==0 && field[row][column-1]==0 && field[row+1][column]==0 -> {
                row+=1
            }
            rc % 4 == 3 && field[row+1][column]==0 && field[row+1][column+1]==0 -> {
                row+=1
            }
            else -> {
                done = true
                field[row][column]=4
                when {
                    rc % 4 == 0 -> {
                        field[row+1][column]=4
                        field[row+1][column+1]=4
                        field[row+1][column+2]=4
                    }
                    rc % 4 == 1 -> {
                        field[row][column-1]=4
                        field[row+1][column-1]=4
                        field[row+2][column-1]=4
                    }
                    rc % 4 == 2 -> {
                        field[row-1][column]=4
                        field[row-1][column-1]=4
                        field[row-1][column-2]=4
                    }
                    rc % 4 == 3 -> {
                        field[row][column+1]=4
                        field[row-1][column+1]=4
                        field[row-2][column+1]=4
                    }
                }
            }
        }
    }

    override fun movabletoleft(): Boolean {
        return if(rc % 4 == 0 && field[row][column-1]==0 && field[row+1][column-1]==0) {
            true
        } else if (rc % 4 == 1 && field[row][column-2]==0 && field[row+1][column-2]==0 && field[row+2][column-2]==0) {
            true
        } else if (rc % 4 == 2 && field[row][column-1]==0 && field[row-1][column-3]==0) {
            true
        } else rc % 4 == 3 && field[row][column-1]==0 && field[row-1][column]==0 && field[row-2][column]==0
    }

    override fun movabletoright(): Boolean {
        return if (rc % 4 == 0 && field[row+1][column+3]==0 && field[row][column+1]==0) {
            true
        } else if(rc % 4 == 1 && field[row][column+1]==0 && field[row+1][column]==0 && field[row+2][column+1]==0) {
            true
        } else if(rc % 4 == 2 && field[row][column+1]==0 && field[row-1][column+1]==0) {
            true
        } else rc % 4 == 3 && field[row][column+2]==0 && field[row-1][column+2]==0 && field[row-2][column+2]==0
    }

    override fun moveleft() {
        if(movabletoleft()) {
            column -= 1
        }
    }

    override fun moveright() {
        if(movabletoright()) {
            column += 1
        }
    }

    override fun rotate() {
        if(!(rc % 4 == 3 && field[row][column+2]!=0) && !(rc % 4 == 1 && field[row][column-2]!=0)) {
            rc+=1
            when {
                rc % 4 == 1 -> {
                    column += 2
                }
                rc % 4 == 2 -> {
                    row += 2
                }
                rc % 4 == 3 -> {
                    column -= 2

                }
                rc % 4 == 0 -> {
                    row -= 2
                }
            }
        }
    }
}

class Lblock(field: Array<Array<Int>>) : Renderable, Blocks(field) {

    override fun render(canvas: Canvas) {
        val context = canvas.graphicsContext2D
        context.fill = Color.ORANGE
        context.fillRect((column+1)*1.5+(column*30), (row+1)*1.5+(row*30), WIDTH, HEIGHT)
        when {
            rc % 4 == 0 -> {
                for(i in 0 until 3) {
                    context.fillRect(((column-i)+1)*1.5+((column-i)*30), ((row+1)+1)*1.5+((row+1)*30), WIDTH, HEIGHT)
                }
            }
            rc % 4 == 1 -> {
                for(i in 0 until 3) {
                    context.fillRect(((column-1)+1)*1.5+((column-1)*30), ((row-i)+1)*1.5+((row-i)*30), WIDTH, HEIGHT)
                }
            }
            rc % 4 == 2 -> {
                for(i in 0 until 3) {
                    context.fillRect(((column+i)+1)*1.5+((column+i)*30), ((row-1)+1)*1.5+((row-1)*30), WIDTH, HEIGHT)
                }
            }
            rc % 4 == 3 -> {
                for(i in 0 until 3) {
                    context.fillRect(((column+1)+1)*1.5+((column+1)*30), ((row+i)+1)*1.5+((row+i)*30), WIDTH, HEIGHT)
                }
            }
        }
    }

    override fun step() {
        when {
            rc % 4 == 0 && field[row+2][column]==0 && field[row+2][column-1]==0 && field[row+2][column-2]==0 -> {
                row+=1
            }
            rc % 4 == 1 && field[row+1][column-1]==0 && field[row+1][column]==0 -> {
                row+=1
            }
            rc % 4 == 2 && field[row][column+2]==0 && field[row][column+1]==0 && field[row+1][column]==0 -> {
                row+=1
            }
            rc % 4 == 3 && field[row+1][column]==0 && field[row+3][column+1]==0 -> {
                row+=1
            }
            else -> {
                done = true
                field[row][column]=5
                when {
                    rc % 4 == 0 -> {
                        field[row+1][column]=5
                        field[row+1][column-1]=5
                        field[row+1][column-2]=5
                    }
                    rc % 4 == 1 -> {
                        field[row][column-1]=5
                        field[row-1][column-1]=5
                        field[row-2][column-1]=5
                    }
                    rc % 4 == 2 -> {
                        field[row-1][column]=5
                        field[row-1][column+1]=5
                        field[row-1][column+2]=5
                    }
                    rc % 4 == 3 -> {
                        field[row][column+1]=5
                        field[row+1][column+1]=5
                        field[row+2][column+1]=5
                    }
                }
            }
        }
    }

    override fun movabletoleft(): Boolean {
        return if(rc % 4 == 0 && field[row][column-3]==0 && field[row+1][column-3]==0) {
            true
        } else if (rc % 4 == 1 && field[row][column-2]==0 && field[row-1][column-2]==0 && field[row-2][column-2]==0) {
            true
        } else if (rc % 4 == 2 && field[row][column-1]==0 && field[row-1][column-1]==0) {
            true
        } else rc % 4 == 3 && field[row][column-1]==0 && field[row-1][column]==0 && field[row-2][column]==0
    }

    override fun movabletoright(): Boolean {
        return if(rc % 4 == 0 && field[row][column+1]==0 && field[row+1][column+1]==0) {
            true
        } else if(rc % 4 == 1 && field[row][column+1]==0 && field[row-1][column]==0 && field[row-2][column]==0) {
            true
        } else if(rc % 4 == 2 && field[row][column+1]==0 && field[row-1][column+3]==0) {
            true
        } else rc % 4 == 3 && field[row][column+2]==0 && field[row+1][column+2]==0 && field[row+2][column+2]==0
    }

    override fun moveleft() {
        if(movabletoleft()) {
            column -= 1
        }
    }

    override fun moveright() {
        if(movabletoright()){
            column += 1
        }
    }

    override fun rotate() {
        if(!(rc % 4 == 3 && field[row][column+2]!=0) && !(rc % 4 == 1 && field[row][column-2]!=0)) {
            rc+=1
            when {
                rc % 4 == 1 -> {
                    row += 2
                }
                rc % 4 == 2 -> {
                    column -= 2
                }
                rc % 4 == 3 -> {
                    row -= 2
                }
                rc % 4 == 0 -> {
                    column += 2
                }
            }
        }
    }
}

class Sblock(field: Array<Array<Int>>) : Renderable, Blocks(field) {

    override fun render(canvas: Canvas) {
        val context = canvas.graphicsContext2D
        context.fill = Color.GREEN
        context.fillRect((column+1)*1.5+(column*30), (row+1)*1.5+(row*30), WIDTH, HEIGHT)
        when {
            rc % 2 == 0 -> {
                context.fillRect(((column-1)+1)*1.5+((column-1)*30), (row+1)*1.5+(row*30), WIDTH, HEIGHT)
                for(i in 0 until 2) {
                    context.fillRect(((column-1-i)+1)*1.5+((column-1-i)*30), ((row+1)+1)*1.5+((row+1)*30), WIDTH, HEIGHT)
                }
            }
            rc % 2 == 1 -> {
                context.fillRect((column+1)*1.5+(column*30), ((row-1)+1)*1.5+((row-1)*30), WIDTH, HEIGHT)
                for(i in 0 until 2) {
                    context.fillRect(((column-1)+1)*1.5+((column-1)*30), ((row-1-i)+1)*1.5+((row-1-i)*30), WIDTH, HEIGHT)
                }
            }
        }
    }

    override fun step() {
        when {
            rc % 2 == 0 && field[row+1][column]==0 && field[row+2][column-1]==0 && field[row+2][column-2]==0 -> {
                row+=1
            }
            rc % 2 == 1 && field[row+1][column]==0 && field[row][column-1]==0 -> {
                row+=1
            }
            else -> {
                done = true
                field[row][column]=6
                if(rc % 2 == 0) {
                    field[row][column-1]=6
                    field[row+1][column-1]=6
                    field[row+1][column-2]=6
                }
                else if (rc % 2 == 1) {
                    field[row-1][column]=6
                    field[row-1][column-1]=6
                    field[row-2][column-1]=6
                }
            }
        }
    }

    override fun movabletoleft(): Boolean {
        return if(rc % 2 == 0 && field[row+1][column-3]==0 && field[row][column-2]==0) {
            true
        } else rc % 2 == 1 && field[row-2][column-2]==0 && field[row-1][column-2]==0 && field[row][column-1]==0
    }

    override fun movabletoright(): Boolean {
        return if(rc % 2 == 0 && field[row+1][column]==0 && field[row][column+1]==0) {
            true
        } else rc % 2 == 1 && field[row-2][column]==0 && field[row-1][column+1]==0 && field[row][column+1]==0
    }

    override fun moveleft() {
        if(movabletoleft()) {
            column -= 1
        }
    }

    override fun moveright() {
        if(movabletoright()) {
            column += 1
        }
    }

    override fun rotate() {
        if(!(rc % 2 == 1 && field[row][column+1]!=0) && !(rc % 2 == 1 && field[row][column-2]!=0)) {
            rc+=1
            when {
                rc % 2 == 0 -> {
                    row -= 2
                }
                rc % 2 == 1 -> {
                    row += 2
                }
            }
        }
    }
}

class Zblock(field: Array<Array<Int>>) : Renderable, Blocks(field) {

    override fun render(canvas: Canvas) {
        val context = canvas.graphicsContext2D
        context.fill = Color.RED
        context.fillRect((column+1)*1.5+(column*30), (row+1)*1.5+(row*30), WIDTH, HEIGHT)
        when {
            rc % 2 == 0 -> {
                context.fillRect(((column+1)+1)*1.5+((column+1)*30), (row+1)*1.5+(row*30), WIDTH, HEIGHT)
                for(i in 0 until 2) {
                    context.fillRect(((column+1+i)+1)*1.5+((column+1+i)*30), ((row+1)+1)*1.5+((row+1)*30), WIDTH, HEIGHT)
                }
            }
            rc % 2 == 1 -> {
                context.fillRect((column+1)*1.5+(column*30), ((row-1)+1)*1.5+((row-1)*30), WIDTH, HEIGHT)
                for(i in 0 until 2) {
                    context.fillRect(((column+1)+1)*1.5+((column+1)*30), ((row-1-i)+1)*1.5+((row-1-i)*30), WIDTH, HEIGHT)
                }
            }
        }
    }

    override fun step() {
        when {
            rc % 2 == 0 && field[row+1][column]==0 && field[row+2][column+1]==0 && field[row+2][column+2]==0 -> {
                row+=1
            }
            rc % 2 == 1 && field[row+1][column]==0 && field[row][column+1]==0 -> {
                row+=1
            }
            else -> {
                done = true
                field[row][column]=7
                if(rc % 2 == 0) {
                    field[row][column+1]=7
                    field[row+1][column+1]=7
                    field[row+1][column+2]=7
                }
                else if (rc % 2 == 1) {
                    field[row-1][column]=7
                    field[row-1][column+1]=7
                    field[row-2][column+1]=7
                }
            }
        }
    }

    override fun movabletoleft(): Boolean {
        return if(rc % 2 == 0 && field[row+1][column]==0 && field[row][column-1]==0) {
            true
        } else rc % 2 == 1 && field[row-2][column]==0 && field[row-1][column-1]==0 && field[row][column-1]==0
    }

    override fun movabletoright(): Boolean {
        return if(rc % 2 == 0 && field[row+1][column+3]==0 && field[row][column+2]==0) {
            true
        } else rc % 2 == 1 && field[row-2][column+2]==0 && field[row-1][column+2]==0 && field[row][column+1]==0
    }

    override fun moveleft() {
        if(movabletoleft()) {
            column -= 1
        }
    }

    override fun moveright() {
        if(movabletoright()) {
            column += 1
        }
    }

    override fun rotate() {
        if(!(rc % 2 == 1 && field[row][column-1]!=0) && !(rc % 2 == 1 && field[row][column+2]!=0)) {
            rc+=1
            when {
                rc % 2 == 0 -> {
                    row -= 2
                }
                rc % 2 == 1 -> {
                    row += 2
                }
            }
        }
    }
}

class Tblock(field: Array<Array<Int>>) : Renderable, Blocks(field) {

    override fun render(canvas: Canvas) {
        val context = canvas.graphicsContext2D
        context.fill = Color.PURPLE
        context.fillRect((column+1)*1.5+(column*30), (row+1)*1.5+(row*30), WIDTH, HEIGHT)
        when {
            rc % 4 == 0 -> {
                for(i in 0 until 3) {
                    context.fillRect(((column-1+i)+1)*1.5+((column-1+i)*30), ((row+1)+1)*1.5+((row+1)*30), WIDTH, HEIGHT)
                }
            }
            rc % 4 == 1 -> {
                for(i in 0 until 3) {
                    context.fillRect(((column-1)+1)*1.5+((column-1)*30), ((row-1+i)+1)*1.5+((row-1+i)*30), WIDTH, HEIGHT)
                }
            }
            rc % 4 == 2 -> {
                for(i in 0 until 3) {
                    context.fillRect(((column-1+i)+1)*1.5+((column-1+i)*30), ((row-1)+1)*1.5+((row-1)*30), WIDTH, HEIGHT)
                }
            }
            rc % 4 == 3 -> {
                for(i in 0 until 3) {
                    context.fillRect(((column+1)+1)*1.5+((column+1)*30), ((row-1+i)+1)*1.5+((row-1+i)*30), WIDTH, HEIGHT)
                }
            }
        }
    }

    override fun step() {
        when {
            rc % 4 == 0 && field[row+2][column]==0 && field[row+2][column-1]==0 && field[row+2][column+1]==0 -> {
                row+=1
            }
            rc % 4 == 1 && field[row+1][column]==0 && field[row+2][column-1]==0 -> {
                row+=1
            }
            rc % 4 == 2 && field[row+1][column]==0 && field[row][column-1]==0 && field[row][column+1]==0 -> {
                row+=1
            }
            rc % 4 == 3 && field[row+1][column]==0 && field[row+2][column+1]==0 -> {
                row+=1
            }
            else -> {
                done = true
                field[row][column]=8
                when {
                    rc % 4 == 0 -> {
                        field[row+1][column]=8
                        field[row+1][column-1]=8
                        field[row+1][column+1]=8
                    }
                    rc % 4 == 1 -> {
                        field[row][column-1]=8
                        field[row+1][column-1]=8
                        field[row-1][column-1]=8
                    }
                    rc % 4 == 2 -> {
                        field[row-1][column]=8
                        field[row-1][column-1]=8
                        field[row-1][column+1]=8
                    }
                    rc % 4 == 3 -> {
                        field[row][column+1]=8
                        field[row+1][column+1]=8
                        field[row-1][column+1]=8
                    }
                }
            }
        }
    }

    override fun movabletoleft(): Boolean {
        return if(rc % 4 == 0 && field[row][column-1]==0 && field[row+1][column-2]==0) {
            true
        } else if(rc % 4 == 1 && field[row-1][column-2]==0 && field[row][column-2]==0 && field[row+1][column-2]==0) {
            true
        } else if(rc % 4 == 2 && field[row][column-1]==0 && field[row-1][column-2]==0) {
            true
        } else rc % 4 == 3 && field[row-1][column]==0 && field[row][column-1]==0 && field[row+1][column]==0
    }

    override fun movabletoright(): Boolean {
        return if(rc % 4 == 0 && field[row][column+1]==0 && field[row+1][column+2]==0) {
            true
        } else if(rc % 4 == 1 && field[row-1][column]==0 && field[row][column+1]==0 && field[row+1][column]==0) {
            true
        } else if(rc % 4 == 2 && field[row][column+1]==0 && field[row-1][column+2]==0) {
            true
        } else rc % 4 == 3 && field[row-1][column+2]==0 && field[row][column+2]==0 && field[row+1][column+2]==0
    }

    override fun moveleft() {
        if(movabletoleft()) {
            column -= 1
        }
    }

    override fun moveright() {
        if(movabletoright()) {
            column += 1
        }
    }

    override fun rotate() {
        if(!(rc % 4 == 1 && field[row][column+1]!=0) && !(rc % 4 == 3 && field[row][column-1]!=0)) {
            rc+=1
        }
    }
}