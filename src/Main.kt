package com.example

import javafx.application.Application

fun main(args: Array<String>) {
    Application.launch(TetrisApplication::class.java, *args)
}
